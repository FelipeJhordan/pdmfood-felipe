package felipe.pdm.pdmfood.ui.activities

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.data.DAOStoresSingleton
import felipe.pdm.pdmfood.model.Store
import felipe.pdm.pdmfood.ui.activities.lists.stores.StoresAdapter

class ListStoreActivity : AppCompatActivity() {

    private lateinit var lvStores: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_store)
        this.lvStores = findViewById(R.id.lvStores)

        var  adapter: StoresAdapter = StoresAdapter( DAOStoresSingleton.getStores())
        this.lvStores.adapter = adapter
        this.lvStores.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            var store:Store = parent.getItemAtPosition(position) as Store
            showStoreInfo(store)
        }
    }

    private fun showStoreInfo(store: Store) {
        var dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(R.string.store_info)
        var view: View =
            LayoutInflater.from(this).inflate(R.layout.show_store_info_dialog, null, false)
        dialogBuilder.setView(view)
        var txtStoreName: TextView = view.findViewById(R.id.txtStoreName)
        txtStoreName.text = store.name
        var txtOwner: TextView = view.findViewById(R.id.txtOwner)
        txtOwner.text = store.owner
        var txtSsn: TextView = view.findViewById(R.id.txtSsn)
        txtSsn.text = store.ssn
        var txtPhone: TextView = view.findViewById(R.id.txtPhone)
        txtPhone.text = store.phoneNumber
        var txtEmail: TextView = view.findViewById(R.id.txtEmail)
        txtEmail.text = store.email
        var txtAddress: TextView = view.findViewById(R.id.txtAddress)
        txtAddress.text = store.address
        dialogBuilder.apply {
            setPositiveButton(R.string.btn_ok, DialogInterface.OnClickListener { dialog, which ->
            })
        }
        dialogBuilder.create().show()
    }
}