package felipe.pdm.pdmfood.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.data.DAOStoresSingleton
import felipe.pdm.pdmfood.model.Store

class MainActivity: AppCompatActivity() {

    companion object  {
        const val FORM_REQUEST_CODE = 1578
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DAOStoresSingleton.getStores()
        setContentView(R.layout.activity_main)
    }

    fun onClickNewStore(view: View) {
          var openStoreForms: Intent = Intent(this, NewStoreActivity::class.java)
          startActivityForResult(openStoreForms, FORM_REQUEST_CODE)
    }

    fun onClickListStores(view: View) {
        var listAllStores = Intent(this, ListStoreActivity::class.java)
        startActivity(listAllStores)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if( (requestCode == FORM_REQUEST_CODE) && (resultCode == RESULT_OK) && (data != null) ) {
             var store: Store? = data.getParcelableExtra<Store>(NewStoreActivity.RESULT_KEY)
             DAOStoresSingleton.addStore(store as Store) // store or null também funcionaria ( mas estaria + feio o código )

        }
    }

}