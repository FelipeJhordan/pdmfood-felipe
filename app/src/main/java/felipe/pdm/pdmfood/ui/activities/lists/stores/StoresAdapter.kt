package felipe.pdm.pdmfood.ui.activities.lists.stores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.model.Store

class StoresAdapter: BaseAdapter{

     companion  object  {
         private class ViewHolder {
            lateinit var txtDetailStoreName: TextView
            lateinit var txtDetailAddress: TextView
        }
    }

    private lateinit var stores: ArrayList<Store>

    constructor(stores: ArrayList<Store>){
        this.stores = stores
    }
    override fun getView(position: Int, convertView: View, parent: ViewGroup?): View? {
      var vh: ViewHolder
      if(convertView == null) {
          var inflater = LayoutInflater.from(parent?.context)
          var convertView = inflater.inflate(R.layout.store_item_view, parent, false)
          vh = ViewHolder()
          vh.txtDetailStoreName = convertView.findViewById(R.id.txtDetailStoreName)
          vh.txtDetailAddress = convertView.findViewById(R.id.txtDetailAddress)
          convertView.tag = vh
      } else {
          vh = convertView.tag as ViewHolder
      }


        var store:Store = this.stores.get(position)

        vh.txtDetailAddress.text = (store.address)
        vh.txtDetailStoreName.text = (store.name)

        return convertView
    }

    override fun getItem(position: Int): Any {
        return this.stores.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return this.stores.size
    }
}