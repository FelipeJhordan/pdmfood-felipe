package felipe.pdm.pdmfood.ui.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.model.Store

class NewStoreActivity : AppCompatActivity() {

    companion object {
        val RESULT_KEY = "NewStoreActivity.RESULT_KEY"
    }

    private lateinit var etxtStoreName: TextView
    private lateinit var etxtOwner: TextView
    private lateinit var etxtSsn: TextView
    private lateinit var etxtPhone: TextView
    private lateinit var etxtEmail: TextView
    private lateinit var etxtAddress: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_store)
        this.etxtStoreName = findViewById(R.id.etxtStoreName)
        this.etxtOwner = findViewById(R.id.etxtOwner)
        this.etxtSsn = findViewById(R.id.etxtSsn)
        this.etxtPhone = findViewById(R.id.etxtPhone)
        this.etxtEmail = findViewById(R.id.etxtEmail)
        this.etxtAddress = findViewById(R.id.etxtAddress)
    }

    fun onClickSave(view: View) {
        var storeName: String = this.etxtStoreName.text.toString()
        var owner: String = this.etxtOwner.text.toString()
        var ssn: String = this.etxtSsn.text.toString()
        var phone: String = this.etxtPhone.text.toString()
        var email: String = this.etxtEmail.text.toString()
        var address: String = this.etxtAddress.text.toString()

        if(storeName.isEmpty() || owner.isEmpty() || ssn.isEmpty() ||  phone.isEmpty() || email.isEmpty() || address.isEmpty())  return
        else this.saveStore(Store(storeName, owner, ssn, phone, email, address ))
        finish()
    }

    private fun saveStore( store: Store) {
        val output: Intent = Intent()
        output.putExtra(RESULT_KEY, store)
        setResult(Activity.RESULT_OK, output)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}