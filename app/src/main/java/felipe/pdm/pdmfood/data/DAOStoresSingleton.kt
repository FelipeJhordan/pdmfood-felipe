package felipe.pdm.pdmfood.data

import felipe.pdm.pdmfood.model.Store

object DAOStoresSingleton {
      private var stores: ArrayList<Store> = ArrayList<Store>()

     fun getStores():ArrayList<Store> {
          return this.stores
     }

     fun addStore(store:Store) {
          this.stores.add(store)
     }
}