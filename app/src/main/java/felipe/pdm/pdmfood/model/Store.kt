package felipe.pdm.pdmfood.model

import android.os.Parcel
import android.os.Parcelable

class Store(var name: String?, var owner: String?, var ssn: String?, var phoneNumber: String?, var email: String?, var address: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
        name = parcel.readString(),
        owner = parcel.readString(),
        ssn = parcel.readString(),
        phoneNumber = parcel.readString(),
        email = parcel.readString(),
        address = parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
        dest?.writeString(owner)
        dest?.writeString(ssn)
        dest?.writeString(phoneNumber)
        dest?.writeString(email)
        dest?.writeString(address)
    }

    override fun describeContents(): Int {
        return 0;
    }

    companion object CREATOR : Parcelable.Creator<Store> {
        override fun createFromParcel(parcel: Parcel): Store {
            return Store(parcel)
        }

        override fun newArray(size: Int): Array<Store?> {
            return arrayOfNulls(size)
        }
    }


}